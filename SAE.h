/*	
	\file : SAE.h
	\author : Liam Monchanin
	Gerer les adhérants d'un complexe sportif ainsi que leurs activités
*/

/*
	\brief:Gerer les adhérants d'un complexe sportif ainsi que leurs activités
*/

int chargementClients(int *t1, int *t2, int *t3, int tmax);
int rechercheAdherent(int *t1,int val,int LENlo,int *trouve);
void choixAdmin(void);
void Admin(void);
void Adherent(void);
int ChoixActivite(int *t1, int *t2, int *t3);
void DonneesAdherent(int *t1, int *t2);
void CreditCarte(int *t1, int *t2);
void SupprimeAdherent(void);