#include <stdio.h>
#include "SAE.h"
#define LEN 100

int chargementClients(int* t1, int* t2, int* t3, int tmax){ //charge les tableaux
	FILE *fe;
	int numClient, points,ptsDepense;
	fe=fopen("Adherents.txt","r");
	if(fe==NULL){
		printf("Probleme ouverture fichier");
	return -1;}
	int i=0;
	fscanf("%d%*c %d%*c %d%*c",&numClient,&points,&ptsDepense);
	while(feof(fe)==0){
		if(i>tmax){
			printf("taille du tableau dépassée \n");
			fclose(fe);
		return-1;}
		t1[i]=numClient;
		t2[i]=points;
		t3[i]=ptsDepense;
		fscanf("%d%*c %d%*c %d%*c",&numClient,&points,&ptsDepense);
		i++;
		}
		fclose(fe);
		return i;
}

int rechercheAdherent(int *t1, int val, int LENlo, int *trouve){//recherche un adherent et affiche sont identifiant
	int i;	
	for (i=0;i<LENlo;i++){
		if (val==t1[i]){
			*trouve=0;
			return i;}
		if (val<t1[i]){
			*trouve=1;
			return i;}
	}	
	return i;
}

void choixAdmin(void){//choix menu admin ou adherent
	char admin;
	printf("Êtes vous administrateur? (O/N)");
	scanf("%c%*c",&admin);
	if(admin=='O')
		Admin;
	if(admin=='N')
		Adherent;
}

void Admin(void){//menu admin
	int choix;
	printf("MENU: \n1)Nouvel adhérent \n2)Créditer une carte \n3)Bloquer une carte \n4)Réactiver une carte \n5)Afficher les données d'un adhérent \n6)Supprimer un adhérent");
	scanf("%d",&choix);
	if(choix==1)
		NouvelAdherent;
	if(choix==2)
		CreditCarte; //en lien avec adherents
	if(choix==3)
		Bloquercarte;
	if(choix==4)
		RectiverCarte;
	if(choix==5)
		DonneesAdherent;//en lien avec adherent
	if(choix==6)
		SupprimeAdherent;	
}

void Adherent(void){//menu adherent
	int choix;
	printf("MENU: \n1)Choix Activité \n2)Afficher les données d'un adhérent \n3)Créditer une carte \n");
	scanf("%d",&choix);
	if(choix==1)
		ChoixActivite;
	if(choix==2)
		DonneesAdherent;//en lien avec admin
	if(choix==3)
		CreditCarte;//en lien avec admin
}

int ChoixActivite(int *t1, int *t2,int *t3){//choix de l'activité
	int activite,trouve,i,id;
	printf("Choisissez une activité: \n1)VolleyBall(30pts) \n2)Natation(15pts) \n3)Randonnée(20pts) \n4)Escalade(35pts) \n");
	scanf("%d",&activite);
	printf("Saisirvotre identifiant");
	scanf("%d",&id);
	i=rechercheAdherent(t1,id,LEN, &trouve);
	if(activite==1){
		t2[i]=t2[i]-30;
		t3[i]=t3[i]+30;}
	if(activite==2){
		t2[i]=t2[i]-15;
		t3[i]=t3[i]+15;}
	if(activite==3){
		t2[i]=t2[i]-20;
		t3[i]=t3[i]+20;}
	if(activite==4){
		t2[i]=t2[i]-35;
		t3[i]=t3[i]+35;}		
	if(t2[i]<0){
		printf("Erreur: crédit insuffisant.");
		if(activite==1){
			t2[i]=t2[i]+30;
			t3[i]=t3[i]-30;}
		if(activite==2){
			t2[i]=t2[i]+15;
			t3[i]=t3[i]-15;}
		if(activite==3){
			t2[i]=t2[i]+20;
			t3[i]=t3[i]-20;}
		if(activite==4){
			t2[i]=t2[i]+35;
			t3[i]=t3[i]-35;}
		return -1;
	if (t3[i]>=100){
		printf("Vous avez gagné 10pts bonus.");
		t3[i]=t3[i]-100;
		t2[i]=t2[i]+10;
	}
	}
	return 0;
}

void DonneesAdherent(int *t1, int *t2){
	int id,trouve,rech,points;
	printf("Saisir un identifiant:");
	scanf("%d",&id);
	rech=rechercheAdherent(t1,id,LEN,&trouve);
	points=t2[rech];
	printf("Vous êtes l'adhérent %d, et vous avez %d points.",id,points);
}

void CreditCarte(int *t1, int*t2){
	int pts,i,trouve,id;
	float prix;
	printf("Saisir identifiant");
	scanf("%d",&id);
	i=rechercheAdherent(t1,id,LEN,&trouve);
	printf("Saisir un nombre de points(10pts=5€)");
	scanf("%d",&pts);
	while(pts!=0){
		prix=pts*0.5;
		t2[i]=t2[i]+pts;
		printf("Voulez vous saisir plus de points?(10pts=5€)");
		scanf("%d",&pts);}
	printf("Vous avez acheté %d pts pour %.2f €.",pts,prix);
}

void NouvelAdherent(void){
	int *t1, *t2, *t3, tmax;
	char ouinon;
	printf("Voulez-vous créer un nouvel adhérent ? (o/n) : "); //demande de confirmation
	scanf("%c%*c", &ouinon);
	if (ouinon=='o'){
		chargementClients(*t1, *t2, *t3, tmax);
		t1[tmax+1] = t1[tmax] +1;
		t2[tmax+1] = 0;
		printf("un nouvel adhérent de n°%d à été créé");
	}
	Admin;
}

void SupprimeAdherent(void){
	int *t1, *t2, *t3, tmax, *trouve, pos, i;
	char ouinon;
	int ad;
	printf("Entrez le numéro de l'adhérent à supprimer : ");
	scanf("%d", &ad);
	chargementClients(*t1, *t2, *t3, tmax);
	pos = rechercheAdherent(*t1, ad, tmax, *trouve);
	if (*trouve=0){printf("erreur, adhérent non trouvé");}
	else{
		printf("la carte à supprimer est la carte n°%d. Souhaitez-vous continuer ? (o/n) : ", ad);
		scanf("%c%*c", &ouinon);
		if (ouinon=='o'){
			for (i=pos-1; i<tmax; i++);
				t1[i]=t1[i+1];
		}
		printf("la carte n°%d a bien été supprimée.", ad);

	}
}














